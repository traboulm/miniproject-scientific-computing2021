## Mini Project (Scientific Computing 2021)
### By Mohammad Traboulsi
---


Miniproject for the [scientific computing class 2021][1].

The project is about the [monte-carlo integration method][2]. The notebook includes several animations and figures that explain the method, its advantages and disadvantages and compares it with other methods (trapezoidal integration).

## Installation
---

To install this project, first run the following command:

```
hg clone https://gricad-gitlab.univ-grenoble-alpes.fr/traboulm/miniproject-scientific-computing2021`
```

you need to have python 3+ installed as well as certain prerequisites. To install the required python packages, run:

```
pip install -r requirements.txt
```

Note: you may need to install nodejs and the widget add-ons for jupyter to use the notebook.

## Demo
___

Instead of installing the files locally, you can find a [live demo][3] of the application online. The demo was created using `voila` and the `material` templalte. 

[1]:https://gricad-gitlab.univ-grenoble-alpes.fr/meige-legi/scientific-computing-m2-efm
[2]:https://en.wikipedia.org/wiki/Monte_Carlo_integration
[3]:https://monte-carlo-2021.herokuapp.com/
